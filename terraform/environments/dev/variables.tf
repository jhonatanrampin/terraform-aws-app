variable "aws_region" {
  default = "ap-southeast-2"
}

variable "project_name" {
  default = "jr-labs"
}

variable "environment" {
  default = "dev"
}
