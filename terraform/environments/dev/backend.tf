terraform {
  backend "s3" {
    bucket         = "jr-labs-2021"
    key            = "dev/terraform.tfstate"
    region         = "ap-southeast-2"
    encrypt        = true
    #dynamodb_table = "dev/terraform.tfstate"
  }
}
